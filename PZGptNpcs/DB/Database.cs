﻿using LiteDB;
using PzGptNpc.DB.Models;

namespace PzGptNpc.DB;

public class Database : LiteDatabase
{

    public ILiteCollection<Memory> Memories { get; }
    public ILiteCollection<Visit> Visits { get; }
    
    public Database(string path) : base("Filename=" + Path.Combine(path, "npcs.db") + ";Connection=Shared")
    {
        this.
        Visits = GetCollection<Visit>();
        Memories = GetCollection<Memory>();

        Visits.EnsureIndex(m => m.Npc);
        Visits.EnsureIndex(m => m.Player);
        Visits.EnsureIndex(m => m.DateTime);
        
        Memories.EnsureIndex(m => m.Npc);
        Memories.EnsureIndex(m => m.Player);
    }
}
﻿namespace PzGptNpc.DB.Models;

public class Visit
{
    public int Id { get; set; }
    public string Player { get; set; }
    public string Npc { get; set; }
    public DateTime DateTime { get; set; }
    public int NumberOfMessages { get; set; }
    public Dictionary<string, bool> Flags { get; set; } = new();
}
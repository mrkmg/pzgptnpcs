﻿namespace PzGptNpc.DB.Models;

public class Memory
{
    public int Id { get; set; }
    public string Player { get; set; }
    public string Npc { get; set; }
    public DateTime LastVisit { get; set; }
    public string Details { get; set; }
}
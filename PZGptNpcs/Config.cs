using IniParser;

namespace PzGptNpc;

internal class Config
{
    public readonly string PzLuaDirPath;
    public readonly string ConfigDir;
    public readonly string DataDir;
    public readonly string OpenAiModel;
    public readonly string OpenAiKey;
    public readonly bool NoResponseMode = false;
    
    public Config(string path)
    {
        var parser = new FileIniDataParser();
        var data = parser.ReadFile(path);
        
        if (data == null)
        {
            throw new("Failed to load config file");
        }
        
        if (!data.TryGetKey("App.configsDir", out ConfigDir))
            throw new("Missing App.configsDir in config file");
        
        if (!data.TryGetKey("App.dataDir", out DataDir))
            throw new("Missing App.dataDir in config file");
        
        if (!data.TryGetKey("PZ.luaDir", out PzLuaDirPath))
            throw new("Missing PZ.luaDir in config file");

        if (!data.TryGetKey("OpenAI.key", out OpenAiKey))
            throw new("Missing OpenAI.key in config file");
        
        if (!data.TryGetKey("OpenAI.model", out OpenAiModel))
            throw new("Missing OpenAI.model in config file");
        
        if (!data.TryGetKey("App.noResponseMode", out var noResponseMode) || !bool.TryParse(noResponseMode, out NoResponseMode))
            throw new("Missing App.noResponseMode in config file");
        
        Validate();
    }

    private void Validate()
    {
        if (string.IsNullOrEmpty(PzLuaDirPath))
            throw new("PZ.luaDir is empty");
        
        if (!Directory.Exists(PzLuaDirPath))
            throw new("PZ.luaDir does not exist");
        
        if (string.IsNullOrEmpty(ConfigDir))
            throw new("App.configsDir is empty");
        
        if (!Directory.Exists(ConfigDir))
            throw new("App.configsDir does not exist");
        
        if (string.IsNullOrEmpty(DataDir))
            throw new("App.dataDir is empty");
        
        if (!Directory.Exists(DataDir))
            throw new("App.dataDir does not exist");
        
        if (string.IsNullOrEmpty(OpenAiKey))
            throw new("OpenAI.key is empty");

        if (string.IsNullOrEmpty(OpenAiModel))
            throw new("OpenAI.model is empty");
    }
}
using PzGptNpc.PZ.Events;

namespace PzGptNpc.PZ;

public class CommandReader
{
    private readonly string _path;

    public event Action<PlayerEnteredEvent>? PlayerEntered;
    public event Action<PlayerLeftEvent>? PlayerLeft;
    public event Action<ChatReceivedEvent>? ChatReceived;
    public event Action<SystemEvent>? System;

    public CommandReader(string path)
    {
        _path = path;
        if (!Directory.Exists(_path))
            Directory.CreateDirectory(_path);
    }
    
    public void ReadCommands()
    {
        foreach (var file in Directory.EnumerateFiles(_path))
        {
            if (!TryReadFile(file, out var lines)) continue;
            File.Delete(file);
            var i = 0;
            while (i < lines.Count)
            {
                switch (lines[i])
                {
                    case "ChatMessage":
                        OnChatReceived(new(lines[i + 1], lines[i + 2], lines[i + 3]));
                        i += 4;
                        break;
                    case "PlayerEntered":
                        OnPlayerEntered(new(lines[i + 1], lines[i + 2]));
                        i += 3;
                        break;
                    case "PlayerLeft":
                        OnPlayerLeft(new(lines[i + 1], lines[i + 2]));
                        i += 3;
                        break;
                    case "System":
                        if (!Enum.TryParse(lines[i + 1], out SystemEventType type))
                            throw new($"Unknown system event type {lines[i + 1]}");
                        OnSystem(new(type));
                        i += 2;
                        break;
                    case "":
                        // ignore empty lines
                        i += 1;
                        break;
                    default:
                        throw new($"Unknown command {lines[i]}");
                }
            }
        }
    }

    private bool TryReadFile(string path, out List<string> lines)
    {
        lines = new();
        try
        {
            using var fs = File.Open(path, FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            using var sr = new StreamReader(fs);
            while (!sr.EndOfStream)
            {
                lines.Add(sr.ReadLine()!);
            }
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private void OnPlayerEntered(PlayerEnteredEvent e) => PlayerEntered?.Invoke(e);
    private void OnPlayerLeft(PlayerLeftEvent e) => PlayerLeft?.Invoke(e);
    private void OnChatReceived(ChatReceivedEvent e) => ChatReceived?.Invoke(e);
    private void OnSystem(SystemEvent e) => System?.Invoke(e);
}
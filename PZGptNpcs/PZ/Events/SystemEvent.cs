﻿namespace PzGptNpc.PZ.Events;

public readonly struct SystemEvent
{
    public readonly SystemEventType Type;
    
    public SystemEvent(SystemEventType type)
    {
        Type = type;
    }
}

public enum SystemEventType
{
    Boot,
    NpcList,
}
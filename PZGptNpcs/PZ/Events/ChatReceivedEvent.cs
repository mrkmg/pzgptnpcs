namespace PzGptNpc.PZ.Events;

public readonly struct ChatReceivedEvent
{
    public readonly string Player;
    public readonly string Npc;
    public readonly string Message;
    
    public ChatReceivedEvent(string player, string npc, string message)
    {
        Player = player;
        Npc = npc;
        Message = message;
    }
}
namespace PzGptNpc.PZ.Events;

public struct PlayerEnteredEvent
{
    public readonly string Player;
    public readonly string Npc;
    
    public PlayerEnteredEvent(string player, string npc)
    {
        Player = player;
        Npc = npc;
    }
}
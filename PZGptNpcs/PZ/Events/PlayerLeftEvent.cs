namespace PzGptNpc.PZ.Events;

public struct PlayerLeftEvent
{
    public readonly string Player;
    public readonly string Npc;
    
    public PlayerLeftEvent(string player, string npc)
    {
        Player = player;
        Npc = npc;
    }
}
namespace PzGptNpc.PZ;

public class CommandRunner : IDisposable
{
    private Thread? _thread;
    private bool _exit = true;

    public CommandWriter Writer { get; }
    public CommandReader Reader { get; }
    
    public CommandRunner(string inPath, string outPath)
    {
        Writer = new(outPath);
        Reader = new(inPath);
    }

    public void Start()
    {
        _thread = new(ThreadMain);
        _exit = false;
        _thread.Start();
    }

    public void Stop()
    {
        _exit = true;
        _thread?.Join();
        _thread = null;
    }

    public void Dispose()
    {
        Stop();
    }

    private void ThreadMain()
    {
        Console.WriteLine("Listening for commands...");
        while (!_exit)
        {
            Reader.ReadCommands();
            Thread.Sleep(100);
            Writer.WriteAll();
            Thread.Sleep(100);
        }
    }
}
﻿namespace PzGptNpc.PZ.Commands;

public readonly struct PlayerMessage : IWriteCommand
{
    public readonly string Player;
    public readonly Type MessageType;
    public readonly string Message;
    
    public PlayerMessage(string player, Type messageType, string message)
    {
        Player = player;
        MessageType = messageType;
        Message = message;
        if (!message.EndsWith('\n'))
            Message += '\n';
    }

    public string GetFormattedCommand() => $"PlayerMessage\n{Player}\n{MessageType.ToString()}\n{Message.Count(c => c == '\n')}\n{Message}";

    public string DebugString() => $"PlayerMessage: {MessageType.ToString()} to {Player}: {Message}";
    
    public enum Type
    {
        Informational,
        Positive,
        Warning,
        Error,
    }
}
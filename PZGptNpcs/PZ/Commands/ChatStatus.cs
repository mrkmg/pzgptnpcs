﻿namespace PzGptNpc.PZ.Commands;

public readonly struct ChatStatus : IWriteCommand
{
    public readonly string Npc;
    public readonly string Player;
    public readonly Type MessageType;
    
    public ChatStatus(string npc, string player, Type messageType)
    {
        Npc = npc;
        Player = player;
        MessageType = messageType;
    }

    public string GetFormattedCommand() => $"ChatStatus\n{Npc}\n{Player}\n{MessageType.ToString()}";

    public string DebugString() => $"ChatStatus {Npc} {Player} {MessageType.ToString()}";
    
    public enum Type
    {
        Started,
        Ended,
    }
}
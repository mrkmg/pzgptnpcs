namespace PzGptNpc.PZ.Commands;

public readonly struct ChatMessage : IWriteCommand
{
    public string Npc { get; }
    public string Player { get; }
    public string Message { get; }

    public ChatMessage(string npc, string player, string message)
    {
        Npc = npc;
        Player = player;
        Message = message;
        if (!message.EndsWith("\n"))
            Message += "\n";
    }

    public string DebugString() => $"Chat from {Npc} to {Player}: {Message}";

    public string GetFormattedCommand()
        => $"ChatMessage\n{Npc}\n{Player}\n{Message.Count(c => c == '\n')}\n{Message}";
}
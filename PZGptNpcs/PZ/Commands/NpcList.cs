﻿using System.Text;
using PzGptNpc.NPCs;

namespace PzGptNpc.PZ.Commands;

public class NpcList : IWriteCommand
{
    private readonly IList<Npc> _npcs;

    public NpcList(IList<Npc> npcs)
    {
        _npcs = npcs;
    }

    public string GetFormattedCommand()
    {
        var sb = new StringBuilder();
        sb.AppendLine("NpcList");
        sb.AppendLine(_npcs.Count.ToString());
        foreach (var npc in _npcs)
        {
            sb.AppendLine(npc.Name);
            sb.AppendLine(npc.Type.ToString());
        }
        return sb.ToString();
    }

    public string DebugString() => $"NpcList: {_npcs.Count} npcs";
}
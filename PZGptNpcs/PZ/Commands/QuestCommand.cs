using System.Text;

namespace PzGptNpc.PZ.Commands;

// TODO, make less generic
public class QuestCommand : IWriteCommand
{
    public string Npc { get; }
    public string Player { get; }
    public List<string> QuestData { get; }

    public QuestCommand(string npc, string player, List<string> questData)
    {
        Npc = npc;
        Player = player;
        QuestData = questData;
    }

    public string DebugString()
    {
        return $"QuestCommand: {Npc} {Player}";
    }

    public string GetFormattedCommand()
    {
        var sb = new StringBuilder();
        sb.AppendLine("GiveQuest");
        sb.AppendLine(Npc);
        sb.AppendLine(Player);
        foreach (var data in QuestData)
        {
            sb.AppendLine(data);
        }
        return sb.ToString();
    }
}
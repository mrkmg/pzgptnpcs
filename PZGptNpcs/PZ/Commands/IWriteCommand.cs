namespace PzGptNpc.PZ.Commands;

public interface IWriteCommand
{
    public string GetFormattedCommand();
    public string DebugString();
}
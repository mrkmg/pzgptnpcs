﻿namespace PzGptNpc.PZ.Commands;

public readonly struct HarmPlayer : IWriteCommand
{
    public readonly string Npc;
    public readonly string Player;
    public readonly string Type;
    
    public HarmPlayer(string npc, string player, string type)
    {
        Npc = npc;
        Player = player;
        Type = type;
    }

    public string DebugString() => $"{Npc} harms {Player} with {Type}";

    public string GetFormattedCommand() => $"HarmPlayer\n{Npc}\n{Player}\n{Type}";
}
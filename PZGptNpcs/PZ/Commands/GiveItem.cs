﻿namespace PzGptNpc.PZ.Commands;

public readonly struct GiveItem : IWriteCommand
{
    public readonly string Npc;
    public readonly string Player;
    public readonly string Item;
    public readonly int Count;
    
    public GiveItem(string npc, string player, string item, int count)
    {
        Npc = npc;
        Player = player;
        Item = item;
        Count = count;
    }

    public string DebugString() => $"{Npc} gives {Count} {Item} to {Player}";

    public string GetFormattedCommand() => $"GiveItem\n{Npc}\n{Player}\n{Item}\n{Count}";
}
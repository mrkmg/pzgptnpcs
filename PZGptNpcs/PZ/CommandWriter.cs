using System.Collections.Concurrent;
using PzGptNpc.PZ.Commands;

namespace PzGptNpc.PZ;

public class CommandWriter
{
    private readonly string _path;
    private readonly ConcurrentQueue<string> _pendingWrites = new();
    
    public CommandWriter(string path)
    {
        _path = path;
    }

    public void Send(IWriteCommand command)
    {
        _pendingWrites.Enqueue(command.GetFormattedCommand()); 
    }

    public void WriteAll()
    {
        try
        {
            if ((File.Exists(_path) && new FileInfo(_path).Length > 0) || _pendingWrites.IsEmpty) return;
            
            using var file = File.Open(_path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
            using var sw = new StreamWriter(file);
            while (_pendingWrites.TryDequeue(out var message)) sw.WriteLine(message);
        }
        catch (IOException e)
        {
            Console.WriteLine("Error writing commands: " + e.Message);
        }
    }
}
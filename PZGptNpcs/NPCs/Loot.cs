﻿using IniParser.Model;

namespace PzGptNpc.NPCs;

public class Loot
{
    public string Name { get; }
    public List<LootEntry> Entries { get; } = new();

    public Loot(IniData data)
    {
        if (!data.TryGetKey("Loot.Name", out var name)) throw new("Loot.Name is required");
        Name = name;
        foreach (var section in data.Sections)
        {
            if (!section.SectionName.StartsWith("Group")) continue;
            if (section.Keys["Enabled"] != "true") continue;
            
            if (!data.TryGetKey(section.SectionName + ".Rolls", out var count)) throw new($"{section.SectionName}.Count is required");
            if (!data.TryGetKey(section.SectionName + ".Chance", out var chance)) throw new($"{section.SectionName}.Chance is required");

            Entries.Add(new()
            {
                Rolls = int.Parse(count),
                Chance = double.Parse(chance),
                Items = section.Keys.Where(s => s.KeyName.StartsWith("Item")).Select(s => s.Value).ToList(),
            });
        }
    }

    public List<string> GetItems()
    {
        var rnd = Random.Shared;
        var items = new List<string>();
        foreach (var entry in Entries)
        {
            for(var i = 0; i < entry.Rolls; i++)
            {
                if (rnd.NextDouble() > entry.Chance) continue;
                items.Add(entry.Items[rnd.Next(entry.Items.Count)]);
            }
        }
        return items;
    }
}

public class LootEntry
{
    public required int Rolls { get; init; }
    public required double Chance { get; init; }
    public required List<string> Items { get; init; }
}
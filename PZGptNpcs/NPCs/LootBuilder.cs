﻿using IniParser;

namespace PzGptNpc.NPCs;

public static class LootBuilder
{
    public static IEnumerable<Loot> BuildAll(string configsPath)
    {
        var files = Directory.GetFiles(configsPath, "*.ini");
        var iniParser = new FileIniDataParser();
        foreach (var file in files)
        {
            var data = iniParser.ReadFile(file);
            yield return new(data);
        }
    }
}
﻿using IniParser;

namespace PzGptNpc.NPCs;

public static class ZoneBuilder
{
    public static IEnumerable<Zone> BuildAll(string configsPath)
    {
        var files = Directory.GetFiles(configsPath, "*.ini");
        var iniParser = new FileIniDataParser();
        foreach (var file in files)
        {
            var data = iniParser.ReadFile(file);
            yield return data["Zone"]["Type"] switch
            {
                "Location" => new LocationZone(data),
                "Area" => new AreaZone(data),
                _ => throw new ArgumentOutOfRangeException(nameof(configsPath)),
            };
        }
    }
}
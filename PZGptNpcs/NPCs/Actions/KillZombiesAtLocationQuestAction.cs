﻿using System.Text;
using IniParser.Model;
using PzGptNpc.DB;
using PzGptNpc.PZ.Commands;

namespace PzGptNpc.NPCs.Actions;

public class KillZombiesAtLocationQuestAction : ConfiguredAction
{
    private Loot Loot;
    private Zone Zone;
    private QuestRewardType QuestRewardType;
    private int ZombieCount;
    
    public KillZombiesAtLocationQuestAction(SectionData data, IDictionary<string, Loot> loot, IDictionary<string, Zone> zone) : base(data)
    {
        Loot = loot[data.Keys["Loot"]];
        Zone = zone[data.Keys["Zone"]];
        QuestRewardType = Enum.Parse<QuestRewardType>(data.Keys["QuestRewardType"]);
        ZombieCount = int.Parse(data.Keys["ZombieCount"]);
    }

    public override string GetInstruction(object? actionData)
    {
        if (actionData is not InstanceData instanceData) throw new Exception("Invalid action data");
        var instruction = new StringBuilder();
        instruction.Append($"/action {Action} - {Description}");
        switch (instanceData.ZoneType)
        {

            case ZoneType.Location:
                instruction.Append($" The location is called {instanceData.Location?.Title} and is described as {instanceData.Location?.Description}.");
                break;
            case ZoneType.Area:
                instruction.Append($" The area is called {instanceData.Area?.Title}.");
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(actionData));
        }

        switch (QuestRewardType)
        {
            case QuestRewardType.LootStash:
                instruction.Append($" The loot stash is located at {instanceData.Location?.LootDescription}.");
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(QuestRewardType));
        }
        return instruction.ToString();
    }

    public override IWriteCommand GetCommand(string npc, string player, object? actionData = null)
    {
        if (actionData is not InstanceData instanceData) throw new Exception("Invalid action data");


        var zoneData = instanceData.ZoneType switch
        {
            ZoneType.Location => instanceData.Location!.GetCommandFields(),
            ZoneType.Area => instanceData.Area!.GetCommandFields(),
            _ => throw new ArgumentOutOfRangeException(nameof(actionData)),
        };
        var items = Loot.GetItems();

        var list = new List<string>
        {
            "KillZombiesAtLocation",
            ZombieCount.ToString()
        };
        list.AddRange(zoneData);
        list.Add(QuestRewardType.ToString());
        list.Add(items.Count.ToString());
        list.AddRange(items);

        return new QuestCommand(npc, player, list);
    }

    public override object? GetInstanceData(string playerName, Database db) => new InstanceData(Zone);

    private class InstanceData
    {
        public ZoneType ZoneType { get; }
        public Location? Location { get; }
        public Area? Area { get; }

        public InstanceData(Zone zone)
        {
            var rnd = Random.Shared;
            switch (zone)
            {
                case LocationZone locationZone:
                    ZoneType = ZoneType.Location;
                    Location = locationZone.Locations[rnd.Next(locationZone.Locations.Count)];
                    break;
                case AreaZone areaZone:
                    ZoneType = ZoneType.Area;
                    Area = areaZone.Areas[rnd.Next(areaZone.Areas.Count)];
                    break;
                default:
                    throw new Exception("Unknown zone type");
            }
        }
    }
}

public enum QuestRewardType
{
    LootStash,
}
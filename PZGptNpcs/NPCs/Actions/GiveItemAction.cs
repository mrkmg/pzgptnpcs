﻿using IniParser.Model;
using PzGptNpc.PZ.Commands;

namespace PzGptNpc.NPCs.Actions;

public class GiveItemAction : ConfiguredAction
{
    public string Item { get; }
    public int Count { get; }
    
    public GiveItemAction(SectionData data) : base(data)
    {
        Item = data.Keys["Item"];
        Count = int.Parse(data.Keys["Count"]);
    }

    public override IWriteCommand GetCommand(string npc, string player, object? actionData = null) => new GiveItem(npc, player, Item, Count);
}
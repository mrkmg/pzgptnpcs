﻿using IniParser.Model;
using OpenAI_API.Chat;
using PzGptNpc.DB;
using PzGptNpc.PZ.Commands;
using ChatMessage = OpenAI_API.Chat.ChatMessage;

namespace PzGptNpc.NPCs.Actions;

public abstract class ConfiguredAction
{
    public string Action { get; }
    public string Description { get; }
    public ActionLimits Limits { get; }

    public ConfiguredAction(SectionData data)
    {
        Action = data.Keys["ActionKey"];
        Description = data.Keys["Description"];
        Limits = new ActionLimits(data);
    }

    public bool ConversationInvokedThis(Conversation conversation)
        => conversation.Messages.Any(ChatMessageIsAction);
    
    public bool IsAction(string message) => message.Contains("/action " + Action);

    public virtual string GetInstruction(object? actionData) => $"/action {Action} - {Description}";
    
    public virtual object? GetInstanceData(string playerName, Database db) => null;

    public abstract IWriteCommand GetCommand(string npc, string player, object? actionData = null);
    
    public bool IsValidForConversation(Conversation currentConversation) 
        => Limits.SessionLimit <= 0 || currentConversation.Messages.Count(ChatMessageIsAction) <= Limits.SessionLimit;

    public bool IsValidForPlayer(Database db, string npc, string player)
    {
        if (Limits.GlobalLimit <= 0) return true;
        var dtBack = Limits.GlobalCooldownHours > 0
            ? DateTime.UtcNow.AddHours(-Limits.GlobalCooldownHours)
            : DateTime.MinValue;
        return db.Visits
            .Find(v => v.Npc.Equals(npc) && 
                        v.Player.Equals(player) && 
                        v.DateTime > dtBack)
            .Count(v => v.Flags.ContainsKey(Action) && 
                        v.Flags[Action]) < Limits.GlobalLimit;
    }
    
    private bool ChatMessageIsAction(ChatMessage message) => message.Role.Equals(ChatMessageRole.Assistant) && IsAction(message.Content);
}

public class ActionLimits
{
    public int GlobalLimit { get; }
    public int SessionLimit { get; }
    public int GlobalCooldownHours { get; }

    public ActionLimits(SectionData data) 
    {
        GlobalLimit = data.Keys.ContainsKey("GlobalLimit") ? int.Parse(data.Keys["GlobalLimit"]) : 0;
        SessionLimit = data.Keys.ContainsKey("SessionLimit") ? int.Parse(data.Keys["SessionLimit"]) : 0;
        GlobalCooldownHours = data.Keys.ContainsKey("GlobalCooldownHours") ? int.Parse(data.Keys["GlobalCooldownHours"]) : 0;
    }
}

public class ActionInstance
{
    public required ConfiguredAction Action { get; init; }
    public required object? ActionData { get; init; }
}
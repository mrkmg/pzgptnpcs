﻿using IniParser.Model;
using PzGptNpc.PZ.Commands;

namespace PzGptNpc.NPCs.Actions;

public class HarmPlayerAction : ConfiguredAction
{
    private HarmType _harmType;
    
    public HarmPlayerAction(SectionData data) : base(data)
    {
        _harmType = Enum.Parse<HarmType>(data.Keys["HarmType"]);
    }

    public override IWriteCommand GetCommand(string npc, string player, object? actionData = null)
        => new HarmPlayer(npc, player, _harmType.ToString());

    public enum HarmType
    {
        Punch,
        Shoot,
    }
}
using OpenAI_API.Chat;
using PzGptNpc.DB;
using PzGptNpc.DB.Models;
using PzGptNpc.NPCs.Actions;
using PzGptNpc.NPCs.Configs;

namespace PzGptNpc.NPCs;

public class Npc
{
    private readonly string _dataDir;
    private readonly GptManager _gptManager;
    private readonly Dictionary<string, Conversation> _conversations = new();
    private readonly Dictionary<string, List<ActionInstance>> _actionInstances = new();
    private readonly NpcConfiguration _configuration;

    public string Name => _configuration.Name;
    public NpcType Type => _configuration.Type;
    
    public Npc(string dataDir, GptManager gptManager, NpcConfiguration configuration)
    {
        _dataDir = dataDir;
        _gptManager = gptManager;
        _configuration = configuration;
    }

    public async Task<string> RespondToPlayer(string player, string playerMessage)
    {
        if (!_conversations.ContainsKey(player))
            return string.Empty;
        var conversation = _conversations[player];
        
        var count = conversation.Messages.Count(m => m.Role.Equals(ChatMessageRole.User));
        var ratio = (double)count / _configuration.MaxMessagesPerSession;
        if (ratio > 0.8)
        {
            conversation.AppendSystemMessage("The player is almost out of messages. You should end the conversation quickly.");
        }
        else if (count >= _configuration.MaxMessagesPerSession)
        {
            await EndConversation(player);
            return "/me gets tired of talking and ignores you.\n/ooc You have exceeded the maximum number of messages to this NPC. Please try again later.";
        }
        
        conversation.AppendUserInput(playerMessage);
        return await conversation.GetResponseFromChatbotAsync();
    }
    
    public async Task EndConversation(string player)
    {
        if (!_conversations.ContainsKey(player)) 
            return;
        var conversation = _conversations[player];
        
        // If the player didn't actually speak, then don't log the conversation.
        // Return early.
        if (!conversation.Messages.Any(m => m.Role.Equals(ChatMessageRole.User)))
        {
            _actionInstances.Remove(player);
            _conversations.Remove(player);
            return;
        }
        using var db = new Database(_dataDir);
        conversation.AppendSystemMessage(
            "/summary Generate a summary with everything you know about this player so you can recall "
            + "it later. The summary should be one paragraph, with no new lines. Include no prefix or title, just the summary itself.");
        var summary = await conversation.GetResponseFromChatbotAsync();
        db.Memories.DeleteMany(m => m.Npc.Equals(Name) && m.Player.Equals(player));
        db.Memories.Insert(new Memory
        {
            Npc = Name,
            Player = player,
            LastVisit = DateTime.UtcNow,
            Details = summary,
        });
        db.Visits.Insert(new Visit
        {
            Npc = Name,
            Player = player,
            DateTime = DateTime.UtcNow,
            NumberOfMessages = conversation.Messages.Count(m => m.Role.Equals(ChatMessageRole.User)),
            Flags = _configuration.GetFlags(conversation),
        });
        
        _actionInstances.Remove(player);
        _conversations.Remove(player);
    }
    
    public async Task EndAllConversations()
    {
        var conversationEndTasks = _conversations.Keys.Select(EndConversation);
        await Task.WhenAll(conversationEndTasks);
    }

    public NewConversationResult NewConversation(string player)
    {
        using var db = new Database(_dataDir);
        if (_configuration.MaxSessions > 0)
        {
            var dtBack = _configuration.MaxSessionsCooldownHours > 0
                ? DateTime.UtcNow.AddHours(-_configuration.MaxSessionsCooldownHours)
                : DateTime.MinValue;
            var numVisits = db.Visits.Find(v => v.Npc == Name && v.Player == player && v.DateTime > dtBack).Count();
            if (numVisits >= _configuration.MaxSessions)
            {
                var firstVisit = db.Visits.Find(v => v.Npc == Name && v.Player == player && v.DateTime > dtBack).OrderBy(v => v.DateTime).First();
                var timeLeft = firstVisit.DateTime.AddHours(_configuration.MaxSessionsCooldownHours) - DateTime.UtcNow;
                return new()
                {
                    Success = false,
                    ErrorMessage = $"You have already spoken with {Name} {numVisits} times in the last {_configuration.MaxSessionsCooldownHours} hours. Cooldown: {timeLeft.TotalHours:0.0} hours.",
                };
            }
        }
        var actionInstances = _configuration.GetActionInstances(player);
        _actionInstances.Add(player, actionInstances);
        var conversation = _gptManager.CreateConversation();
        _conversations.Add(player, conversation);
        conversation.AppendSystemMessage(_configuration.GetInstructions(actionInstances));
        var summary = db.Memories.FindOne(m => m.Npc.Equals(Name) && m.Player.Equals(player))?.Details;
        if (summary is not null)
            conversation.AppendSystemMessage("Summary of what you know about this player: " + summary);
        else
            conversation.AppendSystemMessage("You have not spoken with this player before.");
        var exampleOutputs = _configuration.GetExampleOutputs();
        foreach (var exampleOutput in exampleOutputs)
            conversation.AppendExampleChatbotOutput(exampleOutput);
        return new() { Success = true };
    }
    
    public Conversation? GetConversation(string player) => _conversations.ContainsKey(player) ? _conversations[player] : null;

    public ConfiguredAction? GetMatchingAction(string message) => _configuration.GetActionCommand(message);
    
    public struct NewConversationResult
    {
        public required bool Success { get; init; }
        public string? ErrorMessage { get; init; }
    }
}
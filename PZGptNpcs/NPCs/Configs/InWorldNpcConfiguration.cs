using IniParser.Model;

namespace PzGptNpc.NPCs.Configs;

public class InWorldNpcConfiguration : NpcConfiguration
{
    public override NpcType Type => NpcType.InWorld;

    public InWorldNpcConfiguration(IniData data, string dataDir, IDictionary<string, Loot> loot, IDictionary<string, Zone> zones) : base(data, dataDir, loot, zones)
    {
    }

    public override List<string> GetExampleOutputs()
    {
        var responses = base.GetExampleOutputs();
        responses.AddRange(new [] {
            "/me looks bored with the man and turns away.\n/end",
        });
        return responses;
    }

    protected override string GetTypeInstructions() => """
        You are in world, as a static, non-moving, NPC, standing near the player.
        You should get bored of the player after about 10 messages, and encourage them to be on their way.
        """;
}
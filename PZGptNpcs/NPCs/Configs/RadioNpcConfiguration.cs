using IniParser.Model;

namespace PzGptNpc.NPCs.Configs;

public class RadioNpcConfiguration : NpcConfiguration
{
    public override NpcType Type => NpcType.Radio;

    public RadioNpcConfiguration(IniData data, string dataDir, IDictionary<string, Loot> loot, IDictionary<string, Zone> zones) : base(data, dataDir, loot, zones)
    {
    }

    public override List<string> GetExampleOutputs()
    {
        var responses = base.GetExampleOutputs();
        responses.AddRange(new [] {
            "/me clicks the radio off.\n/end",
        });
        return responses;
    }

    protected override string GetTypeInstructions() => $"""
            You are speaking to the player over the radio.
            You should not roleplay any actions, only tone of voice and sounds, as the player can not see you.
            You should get bored of the player after about 10 messages, and encourage them to be on their way.
            """;

    public override string GetOverLimitMessage()
    {
        return "/me is barely able to be heard. \"Hello? I <static>n't he<static>r yo<static>....\".\n/end";
    }
}
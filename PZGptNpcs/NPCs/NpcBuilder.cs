using IniParser;
using IniParser.Model;
using PzGptNpc.NPCs.Actions;
using PzGptNpc.NPCs.Configs;

namespace PzGptNpc.NPCs;

public static class NpcBuilder
{
    public static IEnumerable<Npc> BuildAll(string configsPath, string dataDir, GptManager gptManager, IDictionary<string, Loot> loot, IDictionary<string, Zone> zones)
    {
        var files = Directory.GetFiles(configsPath, "*.ini");
        var iniParser = new FileIniDataParser();
        foreach (var file in files)
        {
            var data = iniParser.ReadFile(file);
            if (data["NPC"]["Enabled"] != "true")
                continue;
            yield return BuildNpc(gptManager, dataDir, loot, zones, data);
        }
    }
    
    private static Npc BuildNpc(GptManager gptManager, string dataDir, IDictionary<string, Loot> loot, IDictionary<string, Zone> zones, IniData data)
    {
        var config = BuildConfig(data, dataDir, loot, zones);
        
        return new(dataDir, gptManager, config);
    }

    private static NpcConfiguration BuildConfig(IniData data, string dataDir, IDictionary<string, Loot> loot, IDictionary<string, Zone> zones)
    {
        var type = Enum.Parse<NpcType>(data["NPC"]["Type"]);
        
        NpcConfiguration config = type switch {
            NpcType.InWorld => new InWorldNpcConfiguration(data, dataDir, loot, zones),
            NpcType.Radio => new RadioNpcConfiguration(data, dataDir, loot, zones),
            _ => throw new ArgumentOutOfRangeException(nameof(data)),
        };
        
        return config;
    }
}
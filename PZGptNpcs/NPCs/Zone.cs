﻿using IniParser.Model;

namespace PzGptNpc.NPCs;

public abstract class Zone
{
    public string Name { get; protected set; } = null!;
    public ZoneType Type { get; protected set; }
}


public enum ZoneType
{
    Location,
    Area,
}

public class LocationZone : Zone
{
    public List<Location> Locations { get; }
    public LocationZone(IniData data)
    {
        Type = ZoneType.Location;
        Name = data["Zone"]["Name"];
        Locations = data.Sections
            .Where(s => s.SectionName.StartsWith("Location"))
            .Where(s => s.Keys["Enabled"] == "true")
            .Select(s => new Location
            {
                Title = s.Keys["Title"],
                Description = s.Keys["Description"],
                Position = AreaPosition.Parse(s.Keys["Position"]),
                LootSpawnPoint = PointPosition.Parse(s.Keys["LootSpawnPoint"]),
                LootDescription = s.Keys["LootDescription"],
            }).ToList();
    }
}

public class AreaZone : Zone
{
    public List<Area> Areas { get; }
    public AreaZone(IniData data)
    {
        Type = ZoneType.Area;
        Name = data["Zone"]["Name"];
        Areas = data.Sections.Where(s => s.SectionName.StartsWith("Area")).Select(s => new Area
        {
            Title = s.Keys["Title"],
            Position = AreaPosition.Parse(s.Keys["Position"]),
        }).ToList();
    }
}

public class Location
{
    public required string Title { get; init; }
    public required string Description { get; init; }
    public required AreaPosition Position { get; init; }
    public required PointPosition LootSpawnPoint { get; init; }
    public required string LootDescription { get; init; }

     public List<string> GetCommandFields()
    {
        return new List<string> 
        {
            "Location",
            Title,
            Position.X.ToString(),
            Position.Y.ToString(),
            Position.Z.ToString(),
            LootSpawnPoint.X.ToString(),
            LootSpawnPoint.Y.ToString(),
        };
    }
}

public class Area
{
    public required string Title { get; init; }
    public required AreaPosition Position { get; init; }

    public List<string> GetCommandFields()
    {
        return new List<string> 
        {
            "Area",
            Title,
            Position.X.ToString(),
            Position.Y.ToString(),
            Position.Z.ToString(),
            Position.DX.ToString(),
            Position.DY.ToString(),
        };
    }
}

public class AreaPosition
{
    public required int X { get; init; }
    public required int Y { get; init; }
    public required int Z { get; init; }
    public required int DX { get; init; }
    public required int DY { get; init; }
    
    public static AreaPosition Parse(string position)
    {
        var parts = position.Split(":");
        var point = parts[0].Split(",");
        var delta = parts[1].Split(",");
        return new AreaPosition
        {
            X = int.Parse(point[0]),
            Y = int.Parse(point[1]),
            Z = int.Parse(point[2]),
            DX = int.Parse(delta[0]),
            DY = int.Parse(delta[1]),
        };
    }
}

public class PointPosition
{
    public required int X { get; init; }
    public required int Y { get; init; }
    public required int Z { get; init; }
    
    public static PointPosition Parse(string position)
    {
        var parts = position.Split(",");
        return new PointPosition
        {
            X = int.Parse(parts[0]),
            Y = int.Parse(parts[1]),
            Z = int.Parse(parts[2]),
        };
    }
}
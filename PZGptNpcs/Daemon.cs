using System.Text;
using PzGptNpc.NPCs;
using PzGptNpc.PZ;
using PzGptNpc.PZ.Commands;
using PzGptNpc.PZ.Events;

namespace PzGptNpc;

public class Daemon : IDisposable
{
    private readonly CommandRunner _commandRunner;
    private readonly IDictionary<string, Npc> _npcs;
    private readonly bool _noResponseMode;

    public Daemon(IDictionary<string, Npc> npcs, string inPath, string outPath, bool noResponseMode)
    {
        _npcs = npcs;
        _noResponseMode = noResponseMode;
        _commandRunner = new(inPath, outPath);
        _commandRunner.Reader.PlayerEntered += PlayerEntered;
        _commandRunner.Reader.PlayerLeft += PlayerLeft;
        _commandRunner.Reader.ChatReceived += ChatReceived;
        _commandRunner.Reader.System += SystemRevived;
    }

    private void SystemRevived(SystemEvent obj)
    {
        switch (obj.Type)
        {
            case SystemEventType.Boot:
                Console.WriteLine("Ending all conversations");
                foreach (var npc in _npcs.Values)
                    npc.EndAllConversations().Wait();
                break;
            case SystemEventType.NpcList:
                Console.WriteLine("Sending NPC List");
                WritePossibleNpcs();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void Run(CancellationToken token)
    {
        _commandRunner.Start();
        WritePossibleNpcs();
        if (_noResponseMode)
            Console.WriteLine("NO RESPONSE MODE");
        Console.WriteLine("Running");
        token.WaitHandle.WaitOne();
        _commandRunner.Stop();
    }

    private void PlayerEntered(PlayerEnteredEvent obj)
    {
        Console.WriteLine($"Player {obj.Player} is approaching {obj.Npc}");
        if (!_npcs.TryGetValue(obj.Npc, out var npc))
            return;
        try
        {
            var created = npc.NewConversation(obj.Player);
            if (!created.Success)
            {
                _commandRunner.Writer.Send(new PlayerMessage(obj.Player, PlayerMessage.Type.Warning, created.ErrorMessage!));
                Console.WriteLine("Unable to create conversation");
                return;
            }
        
            _commandRunner.Writer.Send(new ChatStatus(obj.Npc, obj.Player, ChatStatus.Type.Started));
        } catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    private void PlayerLeft(PlayerLeftEvent obj)
    {
        Console.WriteLine($"Player {obj.Player} left the area of {obj.Npc}");
        if (!_npcs.TryGetValue(obj.Npc, out var npc)) return;
        try
        {
            _commandRunner.Writer.Send(new ChatStatus(obj.Npc, obj.Player, ChatStatus.Type.Ended));
            npc.EndConversation(obj.Player).Wait();            
        } catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }

    private void ChatReceived(ChatReceivedEvent obj)
    {
        Console.WriteLine($"Player {obj.Player} said {obj.Message} to {obj.Npc}");
        
        if (_noResponseMode)
        {
            var noResponseCommand = new ChatMessage(obj.Npc, obj.Player, "/say \"Hello, I am a bot. I am not programmed to respond to you.\"");
            Console.WriteLine(noResponseCommand.DebugString());
            _commandRunner.Writer.Send(noResponseCommand);
            return;
        }
        
        if (!_npcs.TryGetValue(obj.Npc, out var npc)) return;

        try
        {
            var task = npc.RespondToPlayer(obj.Player, obj.Message);
            task.Wait();
            foreach (var command in GetCommands(obj.Npc, obj.Player, task.Result))
            {
                Console.WriteLine(command.DebugString());
                _commandRunner.Writer.Send(command);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
        
    }
    
    private IEnumerable<IWriteCommand> GetCommands(string npc, string player, string commandString)
    {
        var current = new StringBuilder();
        IWriteCommand? command;
        foreach (var line in SplitInput(commandString))
        {
            if (line.StartsWith("/") && current.Length > 0)
            {
                command = GetCommand(npc, player, current.ToString());
                if (command is not null)
                    yield return command;
                current.Clear();
            }
            current.AppendLine(line);
        }
        command = GetCommand(npc, player, current.ToString());
        if (command is not null)
            yield return command;
    }

    private IWriteCommand? GetCommand(string npc, string player, string content)
    {
        if (string.IsNullOrWhiteSpace(content))
            return null;
        
        if (!content.StartsWith("/"))
            content = "/say\n" + content;
        
        if (content.ToLowerInvariant().StartsWith("/ooc invalid"))
            return new PlayerMessage(player, PlayerMessage.Type.Error, "You are not allowed to do that.");
        
        if (content.StartsWith("/say") || content.StartsWith("/me") || content.StartsWith("/ooc"))
            return new ChatMessage(npc, player, content);
        
        if (content.StartsWith("/action"))
        {
            var action = _npcs[npc].GetMatchingAction(content);
            if (action is null)
            {
                Console.WriteLine("Unknown action: " + content);
                return null;
            }
            var conversation = _npcs[npc].GetConversation(player);
            if (conversation is null)
            {
                Console.WriteLine("No conversation for player: " + player);
                return null;
            }
            if (!action.IsValidForConversation(conversation))
            {
                return new PlayerMessage(player, PlayerMessage.Type.Informational, "The NPC tried to do something, but due to limits, it failed.");
            }
            return action.GetCommand(npc, player);
        }

        Console.WriteLine("Unknown command: " + content);
        return null;
    }

    private void WritePossibleNpcs()
    {
        _commandRunner.Writer.Send(new NpcList(_npcs.Values.ToList()));
    }

    public void Dispose()
    {
        _commandRunner.Dispose();
    }

    private IEnumerable<string> SplitInput(string message)
    {
        var sb = new StringBuilder();
        foreach (var letter in message)
        {
            if (letter == '\n')
            {
                if (sb.Length > 0)
                {
                    yield return sb.ToString();
                    sb.Clear();                    
                }
            }
            if (letter == '/' && sb.Length > 0)
            {
                yield return sb.ToString();
                sb.Clear();
                sb.Append(letter);
            }
            else
            {
                sb.Append(letter);
            }
        }
        if (sb.Length > 0)
            yield return sb.ToString();
    }
}
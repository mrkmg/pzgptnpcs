using OpenAI_API;
using OpenAI_API.Chat;

namespace PzGptNpc;

public class GptManager
{
    private readonly string _model;
    private readonly OpenAIAPI _api;
    
    public GptManager(string key, string model)
    {
        _model = model;
        _api = new(key);
    }
    
    public Conversation CreateConversation()
    {
        var param = new ChatRequest
        {
            Model = _model,
        };
        return _api.Chat.CreateConversation(param);
    }
}
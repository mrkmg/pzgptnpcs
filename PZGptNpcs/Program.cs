﻿using CommandLine;
using PzGptNpc;
using PzGptNpc.NPCs;

var cliArgs = Parser.Default.ParseArguments<CommandLineArgs>(args).Value;

if (cliArgs is null)
{
    Console.WriteLine("Failed to parse command line arguments");
    return;
}

var config = new Config(Path.Combine(cliArgs.ConfigPath, "config.ini"));

var gptManager = new GptManager(config.OpenAiKey, config.OpenAiModel);
var lootList = LootBuilder.BuildAll(Path.Combine(config.ConfigDir, "loot")).ToDictionary(x => x.Name, x => x);
var zoneList = ZoneBuilder.BuildAll(Path.Combine(config.ConfigDir, "zones")).ToDictionary(x => x.Name, x => x);
var npcList = NpcBuilder.BuildAll(Path.Combine(config.ConfigDir, "npcs"), config.DataDir, gptManager, lootList, zoneList).ToDictionary(x => x.Name, x => x);

if (cliArgs.Mode.ToLowerInvariant() == "interactive")
{
    await new Interactive(npcList).Run();
}
else
{
    var inPath = Path.Combine(config.PzLuaDirPath, "npc-in");
    var outPath = Path.Combine(config.PzLuaDirPath, "npc-out.txt");
    using var daemon = new Daemon(npcList, inPath, outPath, config.NoResponseMode);
    var token = new CancellationTokenSource();
    Console.CancelKeyPress += (sender, args) =>
    {
        token.Cancel();
        args.Cancel = true;
    };
    daemon.Run(token.Token);
}

class CommandLineArgs {
    [Value(0, Required = false, Default = "Daemon")]
    public string Mode { get; set; } = null!;

    [Option(shortName: 'c', longName: "config", Required = false)]
    public string ConfigPath { get; set; } = Environment.CurrentDirectory;
}
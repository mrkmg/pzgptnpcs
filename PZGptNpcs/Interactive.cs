using PzGptNpc.NPCs;

namespace PzGptNpc;

public class Interactive
{
    private IDictionary<string, Npc> _npcs;
    
    public Interactive(IDictionary<string, Npc> npcs)
    {
        _npcs = npcs;
    }

    public async Task Run()
    {
        Console.Write("Your name> ");
        var player = Console.ReadLine() ?? "Unknown Player";

        while (true)
        {
            Console.WriteLine("NPC: ");
            var keys = _npcs.Keys.ToList();
            int i;
            for (i = 0; i < keys.Count; i++)
            {
                Console.WriteLine($"\t{i + 1}: {keys[i]} ");
            }
            Console.WriteLine($"\t{i + 1}: Exit");
            Console.WriteLine();
            Console.Write("Choose One> ");
            if (!int.TryParse(Console.ReadLine() ?? "9999", out var npcIndex))
                continue;
            npcIndex--;
            if (npcIndex >= _npcs.Count)
                break;
            var npc = _npcs[keys[npcIndex]];
            npc.NewConversation(player);
            while (true)
            {
                Console.Write(player + "> ");
                var line = Console.ReadLine();
                if (string.IsNullOrEmpty(line))
                    break;
                if (!line.StartsWith("/"))
                    line = "/me says \"" + line  + "\"";
                var response = await npc.RespondToPlayer(player, line);
                Console.WriteLine();
                Console.WriteLine(npc.Name + ": " + response);
                Console.WriteLine();
            }
            await npc.EndConversation(player);
        }
    } 
}